<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210111140222 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE freelance (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(128) NOT NULL, password VARCHAR(255) NOT NULL, firstname VARCHAR(128) NOT NULL, lastname VARCHAR(128) NOT NULL, birthdate DATE NOT NULL, skills LONGTEXT DEFAULT NULL, known_technologies LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE freelance_profile (freelance_id INT NOT NULL, profile_id INT NOT NULL, INDEX IDX_A53A8C92E8DF656B (freelance_id), INDEX IDX_A53A8C92CCFA12B8 (profile_id), PRIMARY KEY(freelance_id, profile_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE offer (id INT AUTO_INCREMENT NOT NULL, profile_id INT NOT NULL, status_id INT NOT NULL, title VARCHAR(255) NOT NULL, location VARCHAR(255) DEFAULT NULL, client VARCHAR(255) NOT NULL, required_skills LONGTEXT NOT NULL, start_date DATE NOT NULL, desired_technologies LONGTEXT DEFAULT NULL, INDEX IDX_29D6873ECCFA12B8 (profile_id), INDEX IDX_29D6873E6BF700BD (status_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE offer_status (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(64) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE profile (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(128) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE freelance_profile ADD CONSTRAINT FK_A53A8C92E8DF656B FOREIGN KEY (freelance_id) REFERENCES freelance (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE freelance_profile ADD CONSTRAINT FK_A53A8C92CCFA12B8 FOREIGN KEY (profile_id) REFERENCES profile (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE offer ADD CONSTRAINT FK_29D6873ECCFA12B8 FOREIGN KEY (profile_id) REFERENCES profile (id)');
        $this->addSql('ALTER TABLE offer ADD CONSTRAINT FK_29D6873E6BF700BD FOREIGN KEY (status_id) REFERENCES offer_status (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE freelance_profile DROP FOREIGN KEY FK_A53A8C92E8DF656B');
        $this->addSql('ALTER TABLE offer DROP FOREIGN KEY FK_29D6873E6BF700BD');
        $this->addSql('ALTER TABLE freelance_profile DROP FOREIGN KEY FK_A53A8C92CCFA12B8');
        $this->addSql('ALTER TABLE offer DROP FOREIGN KEY FK_29D6873ECCFA12B8');
        $this->addSql('DROP TABLE freelance');
        $this->addSql('DROP TABLE freelance_profile');
        $this->addSql('DROP TABLE offer');
        $this->addSql('DROP TABLE offer_status');
        $this->addSql('DROP TABLE profile');
    }
}
