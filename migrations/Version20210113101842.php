<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210113101842 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE offer_freelance (offer_id INT NOT NULL, freelance_id INT NOT NULL, INDEX IDX_EB6EBBB153C674EE (offer_id), INDEX IDX_EB6EBBB1E8DF656B (freelance_id), PRIMARY KEY(offer_id, freelance_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE offer_freelance ADD CONSTRAINT FK_EB6EBBB153C674EE FOREIGN KEY (offer_id) REFERENCES offer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE offer_freelance ADD CONSTRAINT FK_EB6EBBB1E8DF656B FOREIGN KEY (freelance_id) REFERENCES freelance (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE offer ADD offer_creator_id INT NOT NULL');
        $this->addSql('ALTER TABLE offer ADD CONSTRAINT FK_29D6873EFD88CD72 FOREIGN KEY (offer_creator_id) REFERENCES freelance (id)');
        $this->addSql('CREATE INDEX IDX_29D6873EFD88CD72 ON offer (offer_creator_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE offer_freelance');
        $this->addSql('ALTER TABLE offer DROP FOREIGN KEY FK_29D6873EFD88CD72');
        $this->addSql('DROP INDEX IDX_29D6873EFD88CD72 ON offer');
        $this->addSql('ALTER TABLE offer DROP offer_creator_id');
    }
}
