<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210114135133 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE postulated_offer (id INT AUTO_INCREMENT NOT NULL, freelance_id INT DEFAULT NULL, offer_id INT DEFAULT NULL, state VARCHAR(255) NOT NULL, INDEX IDX_49BBFD1AE8DF656B (freelance_id), INDEX IDX_49BBFD1A53C674EE (offer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE postulated_offer ADD CONSTRAINT FK_49BBFD1AE8DF656B FOREIGN KEY (freelance_id) REFERENCES freelance (id)');
        $this->addSql('ALTER TABLE postulated_offer ADD CONSTRAINT FK_49BBFD1A53C674EE FOREIGN KEY (offer_id) REFERENCES offer (id)');
        $this->addSql('DROP TABLE offer_freelance');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE offer_freelance (offer_id INT NOT NULL, freelance_id INT NOT NULL, INDEX IDX_EB6EBBB153C674EE (offer_id), INDEX IDX_EB6EBBB1E8DF656B (freelance_id), PRIMARY KEY(offer_id, freelance_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE offer_freelance ADD CONSTRAINT FK_EB6EBBB153C674EE FOREIGN KEY (offer_id) REFERENCES offer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE offer_freelance ADD CONSTRAINT FK_EB6EBBB1E8DF656B FOREIGN KEY (freelance_id) REFERENCES freelance (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE postulated_offer');
    }
}
