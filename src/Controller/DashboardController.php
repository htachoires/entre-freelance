<?php


namespace App\Controller;


use App\Entity\Freelance;
use App\Repository\OfferRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractController
{
    /**
     * @Route(path="/mon-tableau-de-bord", name="dashboard")
     *
     * @param OfferRepository     $offerRepository
     *
     * @return Response
     */
    public function dashboard(OfferRepository $offerRepository): Response
    {
        if (!$this->isGranted('ROLE_USER', $this->getUser())) {
            return $this->redirectToRoute('app_login');
        }

        /** @var Freelance $user */
        $user = $this->getUser();

        return $this->render('dashboard.html.twig', [
            'my_offers' => $offerRepository->findAllCreatedOffers($user),
            'my_postulated_offers' => $user->getPostulatedOffers(),
        ]);
    }
}
