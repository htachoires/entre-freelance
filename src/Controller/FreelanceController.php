<?php

namespace App\Controller;

use App\Entity\Freelance;
use App\Form\DeleteAccountType;
use App\Form\FreelanceInformationType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FreelanceController extends AbstractController
{
    const SEPARATOR = '%$%';

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/mon-profil", name="profile", methods={"GET","POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function profile(Request $request): Response
    {
        if (!$this->isGranted('IS_AUTHENTICATED_FULLY', $this->getUser())) {
            return $this->redirectToRoute('app_login');
        }

        $informationForm = $this->createForm(FreelanceInformationType::class, $this->getUser(), [
            'knownTechnologies' => $this->getParameter('technologies')
        ]);
        $informationForm->handleRequest($request);

        $this->processInformationForm($informationForm);

        return $this->render('freelance/profile.html.twig', [
            'information_form' => $informationForm->createView()
        ]);
    }

    /**
     * @Route(path="mon-compte", name="account", methods={"GET"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function account(Request $request): Response
    {
        $deleteAccountForm = $this->createForm(DeleteAccountType::class);

        return $this->render('freelance/account.html.twig', [
            'delete_account' => $deleteAccountForm->createView()
        ]);
    }

    /**
     * @param FormInterface $form
     */
    private function processInformationForm(FormInterface $form)
    {
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Freelance $user */
            $user = $this->getUser();

            $knownTechnologies = $form->get('knownTechnologies')->getData();

            $otherTechnologies = $form->get('otherTechnologies')->getData() ?? [];

            if (!empty($otherTechnologies)) {
                $otherTechnologies = explode(',', $otherTechnologies);
            }

            $user->setKnownTechnologies(implode(self::SEPARATOR, array_merge($otherTechnologies, $knownTechnologies)));

            $user->setSkills($form->get('skills')->getData());

            $this->entityManager->flush();
        }
    }
}
