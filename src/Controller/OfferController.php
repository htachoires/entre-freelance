<?php

namespace App\Controller;

use App\Entity\Freelance;
use App\Entity\Offer;
use App\Entity\OfferStatus;
use App\Entity\PostulatedOffer;
use App\Form\OfferType;
use App\Repository\OfferRepository;
use App\Repository\OfferStatusRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class OfferController extends AbstractController
{
    const SEPARATOR = '%$%';

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param EntityManagerInterface $entityManager
     * @param TranslatorInterface    $translator
     */
    public function __construct(EntityManagerInterface $entityManager, TranslatorInterface $translator)
    {
        $this->entityManager = $entityManager;
        $this->translator = $translator;
    }

    /**
     * @Route("/offre-ajouter", name="offer_add", methods={"GET","POST"})
     *
     * @param Request               $request
     * @param OfferStatusRepository $OfferStatusRepository
     *
     * @return Response
     */
    public function offerAdd(Request $request, OfferStatusRepository $OfferStatusRepository): Response
    {
        $offer = new Offer();
        $offerForm = $this->createForm(OfferType::class, $offer, [
            'knownTechnologies' => $this->getParameter('technologies')
        ]);

        $offerForm->handleRequest($request);

        $res = $this->processOfferForm($offerForm, $OfferStatusRepository, $offer);

        if ($res instanceof RedirectResponse) {
            return $res;
        }

        return $this->render('offer/form.html.twig', [
            'offer_form' => $offerForm->createView(),
            'action' => 'offer.action.add'
        ]);
    }

    /**
     * @Route("/offre-editer-{id}", name="offre_edit", methods={"GET","POST"})
     *
     * @param Request               $request
     * @param Offer                 $offer
     * @param OfferStatusRepository $OfferStatusRepository
     *
     * @return Response
     */
    public function offerEdit(Request $request, Offer $offer, OfferStatusRepository $OfferStatusRepository): Response
    {
        if (!$offer->isCreator($this->getUser()->getId())) {
            return $this->redirectToRoute('dashboard');
        }

        $offerForm = $this->createForm(OfferType::class, $offer, [
            'knownTechnologies' => $this->getParameter('technologies')
        ]);
        $offerForm->handleRequest($request);

        $res = $this->processOfferForm($offerForm, $OfferStatusRepository, $offer);

        if ($res instanceof RedirectResponse) {
            return $res;
        }

        return $this->render('offer/form.html.twig', [
            'offer_form' => $offerForm->createView(),
            'action' => 'offer.action.edit'
        ]);
    }

    /**
     * @Route("/offre-abandonner-{id}", name="offre_cancel", methods={"GET"})
     *
     * @param Offer                 $offer
     * @param OfferStatusRepository $OfferStatusRepository
     *
     * @return RedirectResponse
     */
    public function offerCancel(Offer $offer, OfferStatusRepository $OfferStatusRepository)
    {
        if (!$offer->isCreator($this->getUser()->getId())) {
            return $this->redirectToRoute('dashboard');
        }
        $offerStatus = $OfferStatusRepository->findOneBy(['title' => OfferStatus::CANCELED]);

        $offer->setStatus($offerStatus);

        $this->entityManager->flush();

        return $this->redirectToRoute('dashboard');
    }

    /**
     * @Route("/offre-reafficher-{id}", name="offre_cancel_cancel", methods={"GET"})
     *
     * @param Offer                 $offer
     * @param OfferStatusRepository $OfferStatusRepository
     *
     * @return RedirectResponse
     */
    public function offerCancelCancel(Offer $offer, OfferStatusRepository $OfferStatusRepository)
    {
        if (!$offer->isCreator($this->getUser()->getId())) {
            return $this->redirectToRoute('dashboard');
        }
        $offerStatus = $OfferStatusRepository->findOneBy(['title' => OfferStatus::APPLICANT]);

        $offer->setStatus($offerStatus);

        $this->entityManager->flush();

        return $this->redirectToRoute('dashboard');
    }

    /**
     * @Route("/offre-postuler-{id}", name="offer_applicate", methods={"GET","POST"})
     *
     * @param Offer $offer
     *
     * @return RedirectResponse
     */
    public function offerApplicate(Offer $offer)
    {
        if ($offer->containPostulatedOffer($this->getUser()->getPostulatedOffers())) {

            $postulatedOffer = $this->getUser()->getPostulatedOffers();

            $array = $offer->getApplicants()->filter(function (PostulatedOffer $applicant) use ($postulatedOffer) {
                return $postulatedOffer->contains($applicant);
            });

            if ($offer->getApplicants()->count() == 1) {
                $offerStatus = $this->getDoctrine()->getRepository(OfferStatus::class)->findOneBy(['title' => OfferStatus::APPLICANT]);
                $offer->setStatus($offerStatus);
            }
            $this->addFlash('success', $this->translator->trans('offer.flash.give_up', ['%title%' => $offer->getTitle()]));
            $this->entityManager->remove($array->first());
        } else {
            $postulatedOffer = new PostulatedOffer;

            $postulatedOffer
                ->setFreelance($this->getUser())
                ->setOffer($offer)
                ->setState(PostulatedOffer::WAITING);

            $offerStatus = $this->getDoctrine()->getRepository(OfferStatus::class)->findOneBy(['title' => OfferStatus::POSTULATED]);
            $offer->setStatus($offerStatus);

            $this->entityManager->persist($postulatedOffer);
            $this->addFlash('success', $this->translator->trans('offer.flash.follow', ['%title%' => $offer->getTitle()]));
        }

        $this->entityManager->flush();

        return $this->redirectToRoute('offer_list');
    }

    /**
     * @Route("/liste-des-offres", name="offer_list", methods={"GET","POST"})
     *
     * @param Request         $request
     * @param OfferRepository $offerRepository
     *
     * @return Response
     */
    public function offerList(Request $request, OfferRepository $offerRepository): Response
    {
        $postulatedOffers = [];

        /** @var Freelance|null $user */
        $user = $this->getUser();

        if ($user) {
            $postulatedOffers = $user->getPostulatedOffers();
        }

        $offers = $offerRepository->findAllOffersWithStatus();

        return $this->render('offer/list.html.twig', [
            'offers' => $offers,
            'postulated_offer' => $postulatedOffers
        ]);
    }

    /**
     * @param FormInterface         $form
     * @param OfferStatusRepository $OfferStatusRepository
     * @param Offer                 $offer
     *
     * @return RedirectResponse
     */
    private function processOfferForm(FormInterface $form, OfferStatusRepository $OfferStatusRepository, Offer $offer)
    {
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Freelance $user */

            $knownTechnologies = $form->get('knownTechnologies')->getData();

            $otherTechnologies = $form->get('otherTechnologies')->getData() ?? [];

            if (!empty($otherTechnologies)) {
                $otherTechnologies = explode(',', $otherTechnologies);
            }

            $offer->setDesiredTechnologies(implode(self::SEPARATOR, array_merge($otherTechnologies, $knownTechnologies)));

            $offer->setCreator($this->getUser());

            $offerStatus = $OfferStatusRepository->findOneBy(['title' => OfferStatus::APPLICANT]);
            $offer->setStatus($offerStatus);

            $this->entityManager->persist($offer);
            $this->entityManager->flush();

            return $this->redirectToRoute('dashboard');
        }
    }
}
