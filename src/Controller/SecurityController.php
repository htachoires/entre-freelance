<?php

namespace App\Controller;

use App\Entity\Freelance;
use App\Form\LoginType;
use App\Form\RegisterType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{

    /**
     * @Route("/creer-un-compte", name="register", methods={"GET","POST"})
     *
     * @param Request                      $request
     * @param UserPasswordEncoderInterface $encoder
     *
     * @return Response
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY', $this->getUser())) {
            return $this->redirectToRoute('dashboard');
        }

        $freelance = new Freelance();

        $form = $this->createForm(RegisterType::class, $freelance);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $blankPassword = $freelance->getBlankPassword();

            $encodePassword = $encoder->encodePassword($freelance, $blankPassword);

            $freelance->setPassword($encodePassword);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($freelance);
            $entityManager->flush();

            return $this->redirectToRoute('app_login');
        }

        return $this->render('security/register.html.twig', [
            'freelance' => $freelance,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/se-connecter", name="app_login")
     *
     * @param AuthenticationUtils $authenticationUtils
     *
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY', $this->getUser())) {
            return $this->redirectToRoute('dashboard');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        $loginForm = $this->createForm(LoginType::class);

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'login' => $loginForm->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/delete-account", name="delete_account", methods={"DELETE"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function deleteAccount(Request $request): Response
    {
        if (!$this->isGranted('IS_AUTHENTICATED_FULLY', $this->getUser())) {
            return $this->redirectToRoute('app_login');
        }

        $user = $this->getUser();

        if ($this->isCsrfTokenValid('delete-account', $request->request->get('delete_account')['_token'])) {
            $entityManager = $this->getDoctrine()->getManager();

            $this->get('security.token_storage')->setToken(null);

            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_login');
    }

    /**
     * @Route("/se-deconnecter", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException();
    }
}
