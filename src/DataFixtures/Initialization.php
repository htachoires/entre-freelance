<?php

namespace App\DataFixtures;

use App\Entity\Freelance;
use App\Entity\OfferStatus;
use App\Entity\Profile;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class Initialization extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * Initialization constructor.
     *
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        foreach ([
                     OfferStatus::APPLICANT,
                     OfferStatus::CANCELED,
                     OfferStatus::ACCEPTED,
                     OfferStatus::POSTULATED
                 ] as $offerTitle) {
            $offerStatus = new OfferStatus();
            $offerStatus->setTitle($offerTitle);

            $manager->persist($offerStatus);
        }

        foreach ([
                     Profile::MANAGER,
                     Profile::DEVELOPER,
                     Profile::SCRUM_MASTER,
                     Profile::PRODUCT_OWNER
                 ] as $profileTitle) {
            $profile = new Profile();
            $profile->setTitle($profileTitle);

            $manager->persist($profile);
        }

        $user = new Freelance();

        $user
            ->setLastname('Tarscrum')
            ->setFirstname('Paul')
            ->setBirthdate(new \DateTime("now"))
            ->setEmail('tars@email.com');

        $user->setPassword($this->passwordEncoder->encodePassword($user, 'toto'));

        $manager->persist($user);

        $manager->flush();
    }
}
