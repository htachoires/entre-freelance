<?php

namespace App\Entity;

use App\Repository\FreelanceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=FreelanceRepository::class)
 * @UniqueEntity(fields={"email"})
 */
class Freelance implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column()
     */
    private $firstname;

    /**
     * @ORM\Column()
     */
    private $lastname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date")
     */
    private $birthdate;

    /**
     * @ORM\Column()
     */
    private $email;

    /**
     * @ORM\Column()
     */
    private $password;

    /**
     * @var string
     */
    private $blankPassword;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $skills;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $knownTechnologies;

    /**
     * @ORM\ManyToMany(targetEntity=Profile::class, inversedBy="freelances")
     */
    private $profiles;

    /**
     * @ORM\OneToMany(targetEntity=Offer::class, mappedBy="creator")
     */
    private $createdOffers;

    /**
     * @ORM\OneToMany(targetEntity=PostulatedOffer::class, mappedBy="freelance")
     */
    private $postulatedOffers;

    public function __construct()
    {
        $this->profiles = new ArrayCollection();
        $this->birthdate = new \DateTime();
        $this->createdOffers = new ArrayCollection();
        $this->postulatedOffers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getBirthdate(): ?\DateTimeInterface
    {
        return $this->birthdate;
    }

    public function setBirthdate(\DateTimeInterface $birthdate): self
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * @return Collection|Profile[]
     */
    public function getProfiles(): Collection
    {
        return $this->profiles;
    }

    public function addProfile(Profile $profile): self
    {
        if (!$this->profiles->contains($profile)) {
            $this->profiles[] = $profile;
        }

        return $this;
    }

    public function removeProfile(Profile $profile): self
    {
        $this->profiles->removeElement($profile);

        return $this;
    }

    public function getSkills(): ?string
    {
        return $this->skills;
    }

    public function setSkills(?string $skills): self
    {
        $this->skills = $skills;

        return $this;
    }

    public function getKnownTechnologies(): ?string
    {
        return $this->knownTechnologies;
    }

    public function setKnownTechnologies(?string $knownTechnologies): self
    {
        $this->knownTechnologies = $knownTechnologies;

        return $this;
    }

    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getSalt()
    {
        // This method do nothing
    }

    public function getUsername(): string
    {
        return $this->getEmail();
    }

    public function eraseCredentials()
    {
        // This method do nothing
    }

    /**
     * @return string
     */
    public function getBlankPassword(): string
    {
        return $this->blankPassword;
    }

    /**
     * @param string $blankPassword
     *
     * @return Freelance
     */
    public function setBlankPassword(string $blankPassword): self
    {
        $this->blankPassword = $blankPassword;

        return $this;
    }

    /**
     * @return Collection|Offer[]
     */
    public function getCreatedOffers(): Collection
    {
        return $this->createdOffers;
    }

    public function addCreatedOffer(Offer $createdOffer): self
    {
        if (!$this->createdOffers->contains($createdOffer)) {
            $this->createdOffers[] = $createdOffer;
            $createdOffer->setCreator($this);
        }

        return $this;
    }

    public function removeCreatedOffer(Offer $createdOffer): self
    {
        if ($this->createdOffers->removeElement($createdOffer)) {
            // set the owning side to null (unless already changed)
            if ($createdOffer->getCreator() === $this) {
                $createdOffer->setCreator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PostulatedOffer[]
     */
    public function getPostulatedOffers(): Collection
    {
        return $this->postulatedOffers;
    }

    public function addPostulatedOffer(PostulatedOffer $postulatedOffer): self
    {
        if (!$this->postulatedOffers->contains($postulatedOffer)) {
            $this->postulatedOffers[] = $postulatedOffer;
            $postulatedOffer->setFreelance($this);
        }

        return $this;
    }

    public function removePostulatedOffer(PostulatedOffer $postulatedOffer): self
    {
        if ($this->postulatedOffers->removeElement($postulatedOffer)) {
            // set the owning side to null (unless already changed)
            if ($postulatedOffer->getFreelance() === $this) {
                $postulatedOffer->setFreelance(null);
            }
        }

        return $this;
    }
}
