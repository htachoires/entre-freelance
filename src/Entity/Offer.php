<?php

namespace App\Entity;

use App\Repository\OfferRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=OfferRepository::class)
 */
class Offer
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column()
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     */
    private $title;

    /**
     * @ORM\Column(nullable=true)
     */
    private $location;

    /**
     * @ORM\ManyToOne(targetEntity=Profile::class)
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull()
     */
    private $profile;

    /**
     * @var OfferStatus
     *
     * @ORM\ManyToOne(targetEntity=OfferStatus::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $status;

    /**
     * @ORM\Column()
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     */
    private $client;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $requiredSkills;

    /**
     * @ORM\Column(type="date")
     * @Assert\NotNull()
     */
    private $startDate;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $desiredTechnologies;

    /**
     * @ORM\ManyToOne(targetEntity=Freelance::class, inversedBy="createdOffers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $creator;

    /**
     * @ORM\OneToMany(targetEntity=PostulatedOffer::class, mappedBy="offer")
     * @Assert\NotNull()
     */
    private $applicants;

    public function __construct()
    {
        $this->startDate = new \DateTime("now");
        $this->applicants = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getProfile(): ?Profile
    {
        return $this->profile;
    }

    public function setProfile(?Profile $profile): self
    {
        $this->profile = $profile;

        return $this;
    }

    public function getStatus(): ?OfferStatus
    {
        return $this->status;
    }

    public function setStatus(?OfferStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getClient(): ?string
    {
        return $this->client;
    }

    public function setClient(string $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getRequiredSkills(): ?string
    {
        return $this->requiredSkills;
    }

    public function setRequiredSkills(string $requiredSkills): self
    {
        $this->requiredSkills = $requiredSkills;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getDesiredTechnologies(): ?string
    {
        return $this->desiredTechnologies;
    }

    public function setDesiredTechnologies(?string $desiredTechnologies): self
    {
        $this->desiredTechnologies = $desiredTechnologies;

        return $this;
    }

    public function getCreator(): ?Freelance
    {
        return $this->creator;
    }

    public function setCreator(?Freelance $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    public function isCancel()
    {
        return $this->status->getTitle() === OfferStatus::CANCELED;
    }

    public function getColorStatus()
    {
        switch ($this->status->getTitle()) {
            case OfferStatus::APPLICANT:
                return 'primary';
            case OfferStatus::POSTULATED:
                return 'warning';
            case OfferStatus::ACCEPTED:
                return 'success';
            case OfferStatus::CANCELED:
                return 'danger';
            default:
                throw new \UnexpectedValueException('');
        }
    }

    /**
     * @return Collection|PostulatedOffer[]
     */
    public function getApplicants(): Collection
    {
        return $this->applicants;
    }

    public function addApplicant(PostulatedOffer $applicant): self
    {
        if (!$this->applicants->contains($applicant)) {
            $this->applicants[] = $applicant;
            $applicant->setOffer($this);
        }

        return $this;
    }

    public function removeApplicant(PostulatedOffer $applicant): self
    {
        if ($this->applicants->removeElement($applicant)) {
            // set the owning side to null (unless already changed)
            if ($applicant->getOffer() === $this) {
                $applicant->setOffer(null);
            }
        }

        return $this;
    }

    public function canBePostulated($user): bool
    {
        return ($this->status->getTitle() === OfferStatus::POSTULATED || $this->status->getTitle() === OfferStatus::APPLICANT) && $this->creator->getEmail() !== $user->getEmail();
    }

    public function containPostulatedOffer($postulatedOffer): bool
    {
        return !($this->getApplicants()->filter(function (PostulatedOffer $applicant) use ($postulatedOffer) {
            return $postulatedOffer->contains($applicant);
        })->isEmpty());

    }

    /**
     * @param int|null $userId
     *
     * @return bool
     */
    public function isCreator(?int $userId)
    {
        return $this->creator->getId() === $userId;
    }
}
