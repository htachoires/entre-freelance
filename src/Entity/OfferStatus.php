<?php

namespace App\Entity;

use App\Repository\OfferStatusRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OfferStatusRepository::class)
 */
class OfferStatus
{
    const POSTULATED = 'offer_status.postulated';
    const ACCEPTED = 'offer_status.accepted';
    const CANCELED = 'offer_status.canceled';
    const APPLICANT = 'offer_status.applicant';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column()
     */
    private $title;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }
}
