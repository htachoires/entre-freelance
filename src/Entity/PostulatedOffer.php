<?php

namespace App\Entity;

use App\Repository\PostulatedOfferRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PostulatedOfferRepository::class)
 */
class PostulatedOffer
{
    const ACCEPTED = 'postulated.accepted';
    const REFUSED = 'postulated.refused';
    const WAITING = 'postulated.waiting';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Freelance
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Freelance", inversedBy="postulatedOffers")
     */
    private $freelance;

    /**
     * @var Offer
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Offer", inversedBy="applicants")
     */
    private $offer;

    /**
     * @var string
     *
     * @ORM\Column()
     */
    private $state;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFreelance(): ?Freelance
    {
        return $this->freelance;
    }

    public function setFreelance(?Freelance $freelance): self
    {
        $this->freelance = $freelance;

        return $this;
    }

    public function getOffer(): ?Offer
    {
        return $this->offer;
    }

    public function setOffer(?Offer $offer): self
    {
        $this->offer = $offer;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getColorStatus()
    {
        switch ($this->state) {
            case PostulatedOffer::ACCEPTED:
                return 'success';
            case PostulatedOffer::REFUSED:
                return 'danger';
            case PostulatedOffer::WAITING:
                return 'warning';
            default:
                throw new \UnexpectedValueException('');
        }
    }
}
