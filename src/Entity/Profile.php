<?php

namespace App\Entity;

use App\Repository\ProfileRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProfileRepository::class)
 */
class Profile
{

    const MANAGER = 'profile.manager';
    const DEVELOPER = 'profile.developer';
    const SCRUM_MASTER = 'profile.scrum_master';
    const PRODUCT_OWNER = 'profile.product_owner';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column()
     */
    private $title;

    /**
     * @ORM\ManyToMany(targetEntity=Freelance::class, mappedBy="profiles")
     */
    private $freelances;

    public function __construct()
    {
        $this->freelances = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection|Freelance[]
     */
    public function getFreelances(): Collection
    {
        return $this->freelances;
    }

    public function addFreelance(Freelance $freelance): self
    {
        if (!$this->freelances->contains($freelance)) {
            $this->freelances[] = $freelance;
            $freelance->addProfile($this);
        }

        return $this;
    }

    public function removeFreelance(Freelance $freelance): self
    {
        if ($this->freelances->removeElement($freelance)) {
            $freelance->removeProfile($this);
        }

        return $this;
    }
}
