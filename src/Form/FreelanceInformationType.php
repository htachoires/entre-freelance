<?php


namespace App\Form;


use App\Controller\FreelanceController;
use App\Entity\Freelance;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FreelanceInformationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Freelance $user */
        $user = $builder->getData();

        $technos = explode(FreelanceController::SEPARATOR, $user->getKnownTechnologies());

        $otherTechnos = array_filter($technos, function ($v) {
            $explode = explode('.', $v);

            return count($explode) !== 2 && $explode[0] !== 'technologies';
        });

        $knownTechnos = array_filter($technos, function ($v) {
            $explode = explode('.', $v);
            return count($explode) === 2 && $explode[0] === 'technologies';
        });

        $builder
            ->add('knownTechnologies', ChoiceType::class, [
                'label' => 'freelance.known_technologies',
                'choices' => $options['knownTechnologies'],
                'data' => $knownTechnos,
                'choice_label' => function ($value) {
                    return $value;
                },
                'multiple' => true,
                'mapped' => false,
                'required' => false
            ])
            ->add('otherTechnologies', TextType::class, [
                'label' => false,
                'data' => implode(',', $otherTechnos),
                'help' => 'freelance.other_known_technologies.help',
                'mapped' => false,
                'required' => false
            ])
            ->add('skills', TextareaType::class, [
                'label' => 'freelance.skills',
                'required' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Freelance::class
        ]);

        $resolver->setRequired([
            'knownTechnologies'
        ]);

        $resolver->setAllowedTypes('knownTechnologies', 'array');
    }

}
