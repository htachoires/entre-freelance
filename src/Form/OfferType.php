<?php

namespace App\Form;

use App\Controller\FreelanceController;
use App\Entity\Offer;
use App\Entity\Profile;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class OfferType extends AbstractType
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * OfferType constructor.
     *
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Offer $user */
        $offer = $builder->getData();

        $technos = explode(FreelanceController::SEPARATOR, $offer->getDesiredTechnologies());

        $otherTechnos = array_filter($technos, function ($v) {
            $explode = explode('.', $v);

            return count($explode) !== 2 && $explode[0] !== 'technologies';
        });

        $knownTechnos = array_filter($technos, function ($v) {
            $explode = explode('.', $v);
            return count($explode) === 2 && $explode[0] === 'technologies';
        });

        $builder
            ->add('title', TextType::class, [
                'label' => 'offer.title'
            ])
            ->add('location', TextType::class, [
                'label' => 'offer.location'
            ])
            ->add('client', TextType::class, [
                'label' => 'offer.client'
            ])
            ->add('requiredSkills', TextAreaType::class, [
                'label' => 'offer.required_skill'
            ])
            ->add('startDate', DateType::class, [
                'label' => 'offer.startDate',
                'widget' => 'single_text'
            ])
            ->add('knownTechnologies', ChoiceType::class, [
                'label' => 'freelance.known_technologies',
                'choices' => $options['knownTechnologies'],
                'data' => $knownTechnos,
                'choice_label' => function ($value) {
                    return $value;
                },
                'multiple' => true,
                'mapped' => false,
                'required' => false
            ])
            ->add('otherTechnologies', TextType::class, [
                'label' => false,
                'data' => implode(',', $otherTechnos),
                'help' => 'freelance.other_known_technologies.help',
                'mapped' => false,
                'required' => false
            ])
            ->add('profile', EntityType::class, [
                'label' => 'offer.profile',
                'class' => Profile::class,
                'choice_label' => function (Profile $entity) {
                    return $this->translator->trans($entity->getTitle());
                }
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Offer::class,
        ]);

        $resolver->setRequired([
            'knownTechnologies'
        ]);

        $resolver->setAllowedTypes('knownTechnologies', 'array');
    }
}
