<?php


namespace App\Form;


use App\Entity\Freelance;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', TextType::class, [
                'label' => 'freelance.email',
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('firstname', TextType::class, [
                'label' => 'freelance.firstname',
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('lastname', TextType::class, [
                'label' => 'freelance.lastname',
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('birthdate', DateType::class, [
                'label' => 'freelance.birthdate',
                'widget' => 'single_text',
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('blankPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'constraints' => [
                    new NotBlank()
                ],
                'first_options' => [
                    'label' => 'freelance.first_password'
                ],
                'second_options' => [
                    'label' => 'freelance.second_password'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Freelance::class
        ]);
    }
}
