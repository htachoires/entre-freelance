<?php

namespace App\Repository;

use App\Entity\Freelance;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Freelance|null find($id, $lockMode = null, $lockVersion = null)
 * @method Freelance|null findOneBy(array $criteria, array $orderBy = null)
 * @method Freelance[]    findAll()
 * @method Freelance[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FreelanceRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Freelance::class);
    }

    /**
     * @param Freelance $user
     *
     * @return int|mixed|string
     */
    public function findAllPostulatedOffers(Freelance $user)
    {
        $qb = $this->createQueryBuilder('freelance');

        $qb->join('freelance.postulatedOffers', 'postulated_offers')
            ->join('postulated_offers.status', 'status');

        return $qb->getQuery()->getResult();
    }
}
