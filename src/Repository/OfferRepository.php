<?php

namespace App\Repository;

use App\Entity\Freelance;
use App\Entity\Offer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Offer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Offer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Offer[]    findAll()
 * @method Offer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OfferRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Offer::class);
    }

    /**
     * @return Offer[]
     */
    public function findAllSortByDate()
    {
        return $this->findBy([], ['startDate' => 'DESC']);
    }

    /**
     * @param Freelance $user
     *
     * @return int|mixed|string
     */
    public function findAllCreatedOffers(Freelance $user)
    {
        $qb = $this->createQueryBuilder('offer');

        $qb->addSelect('status')
            ->join('offer.status', 'status')
            ->andWhere($qb->expr()->eq('offer.creator', ':freelance'))
            ->setParameter('freelance', $user);

        return $qb->getQuery()->getResult();
    }

    /**
     * @return int|mixed|string
     */
    public function findAllOffersWithStatus()
    {
        $qb = $this->createQueryBuilder('offer')
            ->orderBy('offer.startDate', 'DESC');

        $qb->addSelect('applicants')
            ->leftjoin('offer.applicants', 'applicants');


        $qb->addSelect('status')
            ->join('offer.status', 'status');

        return $qb->getQuery()->getResult();
    }
}
