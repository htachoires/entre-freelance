<?php

namespace App\Repository;

use App\Entity\PostulatedOffer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PostulatedOffer|null find($id, $lockMode = null, $lockVersion = null)
 * @method PostulatedOffer|null findOneBy(array $criteria, array $orderBy = null)
 * @method PostulatedOffer[]    findAll()
 * @method PostulatedOffer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostulatedOfferRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PostulatedOffer::class);
    }
}
